<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])
    or !isset($_SESSION['questionnaire'])
    or $_SESSION['questionnaire']->idCreateur != $_SESSION['user']->id) {
	header('Location: index.php');
	die();
}
// gestion de l'ajout de question
if (isset($_POST['name'])) {
    $question = new Question( null, $_POST['name'], array(), TypeQuestion::getFromBd($_POST['type']), $_POST['nbPoints'], $_POST['pointsErreur']);

    $reponse1 = new Reponse(null, $_POST['reponse1'], in_array(1, $_POST['reponsesCorrectes'])?true:false);
    $question->listeReponses[] = $reponse1;

    $reponse2 = new Reponse(null, $_POST['reponse2'], in_array(2, $_POST['reponsesCorrectes'])?true:false);
    $question->listeReponses[] = $reponse2;

    if (!empty($_POST['reponse3'])) {
        $reponse3 = new Reponse(null, $_POST['reponse3'], in_array(3, $_POST['reponsesCorrectes'])?true:false);
        $question->listeReponses[] = $reponse3;
    }

    if (!empty($_POST['reponse4'])) {
        $reponse4 = new Reponse( null, $_POST['reponse4'], in_array(4, $_POST['reponsesCorrectes'])?true:false);
        $question->listeReponses[] = $reponse4;
    }

    $_SESSION['questionnaire']->listeQuestions[] = $question;
    $_SESSION['questionnaire']->setToBd();
}

include("head.html");?>
<title>Ajout question</title>
<?php include("navbar.php");?>
<body>
	<main>
        <h1>Questionnaire : <?php echo '<h1>'.$_SESSION['questionnaire']->nomQuestionnaire.'</h1>'; ?></h1>
        <h2>Ajouter question</h2>
        <form action="addQuestion.php" method="post">
            <label for="name">Nom de la question</label>
            <input type="text" name="name" id="name" required>
            <label for="type">Type de la question</label>
            <select name="type" id="type">
                <?php foreach (TypeQuestion::getAllTypeFromBd() as $type) {
                    echo '<option value="'.$type->id.'">'.$type->nomTypeQuestion.'</option>';
                } ?>
            </select>
            <label for="nbPoints">Nombre de Points Réussite</label>
            <input type="number" name="nbPoints" id="nbPoints" required>
            <label for="pointsErreur">Nombre de Points Erreur</label>
            <input type="number" name="pointsErreur" id="pointsErreur" required>
            <label for="reponse1">Réponse1</label>
            <input type="text" name="reponse1" id="reponse1" required>
            <label for="reponse2">Réponse2</label>
            <input type="text" name="reponse2" id="reponse2" required>
            <label for="reponse3">Réponse3</label>
            <input type="text" name="reponse3" id="reponse3">
            <label for="reponse4">Réponse4</label>
            <input type="text" name="reponse4" id="reponse4">
            <label for="reponsesCorrectes">Réponses Correctes</label>
            <input type="checkbox" name="reponsesCorrectes[]" value="1" id="reponse1">
            <input type="checkbox" name="reponsesCorrectes[]" value="2" id="reponse2">
            <input type="checkbox" name="reponsesCorrectes[]" value="3" id="reponse3">
            <input type="checkbox" name="reponsesCorrectes[]" value="4" id="reponse4">
            <input type="submit" value="Créer">
    </main>
</body>
</html>