<?php
// Setup de la connexion à la base de données

require("connect.php");
$dsn="mysql:dbname=".BASE.";port=".PORT.";host=".SERVER;
$connexion;
try{
	$connexion=new PDO($dsn,USER,PASSWD);
	$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
}
catch(PDOException $e){
	printf("Échec de la connexion : %s\n", $e->getMessage());
	exit();
}

// Définition des classes
class User {
	public $id;
	public $email;
	public $motdepasse;

	public function __construct($id, $email, $motdepasse) {
		$this->id = $id;
		$this->email = $email;
		$this->motdepasse = $motdepasse;
	}

	public function __toString() {
		return "User [id=" . $this->id . ", email=" . $this->email . ", motdepasse=" . $this->motdepasse . "]";
	}

	public static function verifyUser($email, $motdepasse) {
		$sql = "SELECT * FROM USER WHERE EMAIL=:email AND MOTDEPASSE=:motdepasse";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':email', $email);
		$requete->bindParam(':motdepasse', $motdepasse);
		$requete->execute();
		$resultat = $requete->fetch(PDO::FETCH_ASSOC);
		if ($resultat) {
			$user = new User($resultat['ID'], $resultat['EMAIL'], $resultat['MOTDEPASSE']);
			return $user;
		} else {
			return null;
		}
	}

	public static function registerUser($email, $motdepasse) {
		$sql = "INSERT INTO USER (EMAIL, MOTDEPASSE) VALUES (:email, :motdepasse)";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':email', $email);
		$requete->bindParam(':motdepasse', $motdepasse);
		$requete->execute();
		$id = $connexion->lastInsertId();
		$user = new User($id, $email, $motdepasse);
		return $user;
	}

	public static function checkExist($email) {
		$sql = "SELECT * FROM USER WHERE EMAIL=:email";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':email', $email);
		$requete->execute();
		$resultat = $requete->fetch(PDO::FETCH_ASSOC);
		if ($resultat) {
			return true;
		} else {
			return false;
		}
	}

	public static function getFromBd($id) {
		$sql = "SELECT * FROM USER WHERE ID=:id";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':id', $id);
		$requete->execute();
		$resultat = $requete->fetch(PDO::FETCH_ASSOC);
		if ($resultat) {
			$user = new User($resultat['ID'], $resultat['EMAIL'], $resultat['MOTDEPASSE']);
			return $user;
		} else {
			return null;
		}
	}

	public function setToBd() {
		if($this->id){
			$sql = "UPDATE USER SET EMAIL=:email, MOTDEPASSE=:motdepasse WHERE ID=:id";
		}
		else{
			$sql = "INSERT INTO USER (EMAIL, MOTDEPASSE) VALUES (:email, :motdepasse)";
		}
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':email', $this->email);
		$requete->bindParam(':motdepasse', $this->motdepasse);
		$requete->execute();
		$this->id = $connexion->lastInsertId();
	}

	public function getQuestionnairesCree() {
		$sql = "SELECT * FROM QUESTIONNAIRE WHERE IDCREATEUR=:id";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':id', $this->id);
		$requete->execute();
		$resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
		$listeQuestionnaires = array();
		foreach ($resultat as $res) {
			$questionnaire = new Questionnaire($res['ID'], $res['NOMQUESTIONNAIRE'], $res['IDCREATEUR'], Question::getFromBd($res["ID"]));
			$listeQuestionnaires[] = $questionnaire;
		}
		return $listeQuestionnaires;
	}
}

class Questionnaire {
	public $id;
	public $nomQuestionnaire;
	public $idCreateur;
	public $listeQuestions;

	public function __construct($id=null, $nomQuestionnaire, $idCreateur, $listeQuestions) {
		$this->id = $id;
		$this->nomQuestionnaire = $nomQuestionnaire;
		$this->idCreateur = $idCreateur;
		$this->listeQuestions = $listeQuestions;
	}

	public static function getQuestionnaires() {
		$sql = "SELECT * FROM QUESTIONNAIRE";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->execute();
		$resultats = $requete->fetchAll(PDO::FETCH_ASSOC);
		$listeQuestionnaires = array();
		foreach ($resultats as $res) {
			$questionnaire = new Questionnaire($res['ID'], $res['NOMQUESTIONNAIRE'], $res['IDCREATEUR'], Question::getFromBd($res["ID"]));
			$listeQuestionnaires[] = $questionnaire;
		}
		return $listeQuestionnaires;
	}

	public static function getFromBd($id) {
		$sql = "SELECT * FROM QUESTIONNAIRE WHERE ID=:id";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':id', $id);
		$requete->execute();
		$resultat = $requete->fetch(PDO::FETCH_ASSOC);
		if ($resultat) {
			$questionnaire = new Questionnaire($resultat['ID'], $resultat['NOMQUESTIONNAIRE'], $resultat['IDCREATEUR'], Question::getFromBd($resultat["ID"]));
			return $questionnaire;
		} else {
			return null;
		}
	}

	public function setToBd() {
		// Quand l'utilisateur crée un questionnaire, il n'a pas encore d'id
		if ($this->id) {
			$sql = "UPDATE QUESTIONNAIRE SET NOMQUESTIONNAIRE=:nomquestionnaire, IDCREATEUR=:idcreateur WHERE ID=:id";
		} else {
			$sql = "INSERT INTO QUESTIONNAIRE (NOMQUESTIONNAIRE, IDCREATEUR) VALUES (:nomquestionnaire, :idcreateur)";
		}
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':nomquestionnaire', $this->nomQuestionnaire);
		$requete->bindParam(':idcreateur', $this->idCreateur);
		if ($this->id) {
			$requete->bindParam(':id', $this->id);
		}
		$requete->execute();
		if (!$this->id){
			$this->id = $connexion->lastInsertId();
		}
		foreach ($this->listeQuestions as $question) {
			$question->setToBd($this->id);
		}
	}

	public static function delFromBd ($id) {
		// on cascade la suppression des questions et des réponses
		$sql = "DELETE FROM QUESTIONNAIRE WHERE ID=:id";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':id', $id);
		$requete->execute();
	}

	public function __toString(){
		return "Questionnaire [id:".$this->id.", nomQuestionnaire:".$this->nomQuestionnaire.", idCreateur:".$this->idCreateur."]";
	}
}

class TypeQuestion {
	public $id;
	public $nomTypeQuestion;

	public static function getFromBd($id) {
		$sql = "SELECT * FROM TYPEQUESTION WHERE ID=:id";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':id', $id);
		$requete->execute();
		$resultat = $requete->fetch(PDO::FETCH_ASSOC);
		if ($resultat) {
			$typeQuestion = new TypeQuestion();
			$typeQuestion->id = $resultat['ID'];
			$typeQuestion->nomTypeQuestion = $resultat['NOMTYPE'];
			return $typeQuestion;
		} else {
			return null;
		}
	}
	public static function getAllTypeFromBd() {
		$sql = "SELECT * FROM TYPEQUESTION";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->execute();
		$resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
		$listeTypeQuestion = array();
		foreach ($resultat as $typeQuestion) {
			$TQ = new TypeQuestion();
			$TQ->id = $typeQuestion['ID'];
			$TQ->nomTypeQuestion = $typeQuestion['NOMTYPE'];
			$listeTypeQuestion[] = $TQ;
		}
		return $listeTypeQuestion;
	}
	public function __toString() {
		return "TypeQuestion [id=" . $this->id . ", nomTypeQuestion=" . $this->nomTypeQuestion . "]";
	}
}

class Question {
	public $id;
	public $intitule;
	public $listeReponses;
	public $typeQuestion;
	public $nombrePointReussite;
	public $nombrePointEchec;

	public function __construct($id=null, $intitule, $listeReponses, $typeQuestion, $nombrePointReussite, $nombrePointEchec) {
		$this->id = $id;
		$this->intitule = $intitule;
		$this->listeReponses = $listeReponses;
		$this->typeQuestion = $typeQuestion;
		$this->nombrePointReussite = $nombrePointReussite;
		$this->nombrePointEchec = $nombrePointEchec;
	}

	public function __toString() {
		return "Question [id=" . $this->id . ", intitule=" . $this->intitule . ", listeReponses=" . $this->listeReponses . ", typeQuestion=" . $this->typeQuestion . ", nombrePointReussite=" . $this->nombrePointReussite . ", nombrePointEchec=" . $this->nombrePointEchec . "]";
	}

	public static function getFromBd($idQuestionnaire) {
		$sql = "SELECT * FROM QUESTION WHERE IDQUESTIONNAIRE=:idQuestionnaire";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':idQuestionnaire', $idQuestionnaire);
		$requete->execute();
		$resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
		$listeQuestions = array();
		foreach ($resultat as $res) {
			$question = new Question($res['ID'], $res['INTITULE'], Reponse::getFromBd($res['ID']), TypeQuestion::getFromBd($res['IDTYPEQUESTION']), $res['NOMBREPOINTREUSSITE'], $res['NOMBREPOINTECHEC']);
			$listeQuestions[] = $question;
		}
		return $listeQuestions;
	}

	public static function getQuestionFromBd($id){
		$sql = "SELECT * FROM QUESTION WHERE ID=:id";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':id', $id);
		$requete->execute();
		$resultat = $requete->fetch(PDO::FETCH_ASSOC);
		if ($resultat){
			return new Question($resultat['ID'], $resultat['INTITULE'], Reponse::getFromBd($resultat['ID']), TypeQuestion::getFromBd($resultat['IDTYPEQUESTION']), $resultat['NOMBREPOINTREUSSITE'], $resultat['NOMBREPOINTECHEC']);
		} else {
			return null;
		}
	}

	public function setToBd($idQuestionnaire) {
		if ($this->id) {
			$sql = "UPDATE QUESTION SET INTITULE=:intitule, IDQUESTIONNAIRE=:idquestionnaire, IDTYPEQUESTION=:idtypequestion, NOMBREPOINTREUSSITE=:nombrepointreussite, NOMBREPOINTECHEC=:nombrepointechec WHERE ID=:id";
		} else {
		$sql = "INSERT INTO QUESTION (INTITULE, IDQUESTIONNAIRE, IDTYPEQUESTION, NOMBREPOINTREUSSITE, NOMBREPOINTECHEC) VALUES (:intitule, :idquestionnaire, :idtypequestion, :nombrepointreussite, :nombrepointechec)";
		}
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':intitule', $this->intitule);
		$requete->bindParam(':idquestionnaire', $idQuestionnaire);
		$requete->bindParam(':idtypequestion', $this->typeQuestion->id);
		$requete->bindParam(':nombrepointreussite', $this->nombrePointReussite);
		$requete->bindParam(':nombrepointechec', $this->nombrePointEchec);
		if ($this->id) {
			$requete->bindParam(':id', $this->id);
		}
		$requete->execute();
		if (!$this->id){
			$this->id = $connexion->lastInsertId();
		}
		foreach ($this->listeReponses as $reponse) {
			$reponse->setToBd($this->id);
		}
	}

	public static function delFromBd($idQuestion) {
		// on cascade la suppression des reponses
		$sql = "DELETE FROM QUESTION WHERE ID=:idQuestion";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':idQuestion', $idQuestion);
		$requete->execute();
	}
}

class Reponse {
	public $id;
	public $texteReponse;
	public $reponseValide;

	public function __construct($id = null, $texteReponse, $reponseValide) {
		$this->id = $id;
		$this->texteReponse = $texteReponse;
		$this->reponseValide = $reponseValide;
	}

	public function __toString() {
		return "Reponse [id=" . $this->id . ", texteReponse=" . $this->texteReponse . ", reponseValide=" . $this->reponseValide . "]";
	}

	public static function getFromBd($idQuestion) {
		$sql = "SELECT * FROM REPONSEQUESTION WHERE IDQUESTION=:idQuestion";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':idQuestion', $idQuestion);
		$requete->execute();
		$resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
		$listeReponses = array();
		foreach ($resultat as $res) {
			$reponse = new Reponse($res['ID'], $res['TEXTEREPONSE'], $res['REPONSEVALIDE']);
			$listeReponses[] = $reponse;
		}
		return $listeReponses;
	}

	public static function getReponseFromBd($id) {
		$sql = "SELECT * FROM REPONSEQUESTION WHERE ID=:id";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':id', $id);
		$requete->execute();
		$resultat = $requete->fetch(PDO::FETCH_ASSOC);
		return $resultat;
	}

	public function setToBd($idQuestion) {
		if ($this->id) {
			$sql = "UPDATE REPONSEQUESTION SET TEXTEREPONSE=:textereponse, IDQUESTION=:idquestion, REPONSEVALIDE=:reponsevalide WHERE ID=:id";
		} else {
			$sql = "INSERT INTO REPONSEQUESTION (TEXTEREPONSE, IDQUESTION, REPONSEVALIDE) VALUES (:textereponse, :idquestion, :reponsevalide)";
		}
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':textereponse', $this->texteReponse);
		$requete->bindParam(':idquestion', $idQuestion);
		$requete->bindParam(':reponsevalide', $this->reponseValide);
		if ($this->id) {
			$requete->bindParam(':id', $this->id);
		}
		$requete->execute();
		if (!$this->id){
			$this->id = $connexion->lastInsertId();
		}
	}

	public static function delFromBd($idReponse) {
		$sql = "DELETE FROM REPONSEQUESTION WHERE ID=:idReponse";
		global $connexion;
		$requete = $connexion->prepare($sql);
		$requete->bindParam(':idReponse', $idReponse);
		$requete->execute();
	}
}

class ReponseUtilisateur {
	public $id;
	public $idQuestion;
	public $idUtilisateur;
	public $idReponse;

}
?>