<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: login.php');
	die();
}
if (isset($_GET['id'])) {
    $questionnaire = Questionnaire::getFromBd($_GET['id']);
    if (!$questionnaire) {
        header('Location: profil.php');
        die();
    }
    $_SESSION['questionnaire'] = $questionnaire;
} else {
    header('Location: questionnaires.php');
    die();
}

$score = 0;
$total = 0;
foreach ($questionnaire->listeQuestions as $q){
    $total += $q->nombrePointReussite;
    if ($q->typeQuestion->nomTypeQuestion == "QCU") {
        $reponse = Reponse::getReponseFromBd($_POST[$q->id]);
        if ($reponse->reponseValide) {
            $score+=$question->nombrePointReussite;
        }
        else{
            $score-=$question->nombrePointEchec;
        }
    }
    else if ($q->typeQuestion->nomTypeQuestion == "QCM") {
        $reponses = Reponse::getFromBd($q->id);
        $bonnesReponses = 0;
        $mauvaisesReponses = 0;
        foreach ($reponses as $r) {
            if (($r->reponseValide && in_array($r->id, $_POST[$q->id])) || (!$r->reponseValide && !in_array($r->id, $_POST[$q->id]))) {
                $bonnesReponses++;
            }
            else{
                $mauvaisesReponses++;
            }
        }
        echo $bonnesReponses;
        echo $mauvaisesReponses;
        if ($mauvaisesReponses == 0) {
            $score += $q->nombrePointReussite;
        }
        else{
            $score -= $q->nombrePointEchec;
        }
    }
}

include("head.html");?>
<title>Questionnaires</title>
<?php include("navbar.php");?>
<body>
    <main>
        <h1>Questionnaire <?php echo $questionnaire->nomQuestionnaire; ?></h1>
        <h2>Votre score est de</h2>
        <h3><?php echo $score."/".$total; ?></h3>
    </main>
</body>