<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: index.php');
	die();
}
// gestion de la création de questionnaire
if (isset($_POST['name'])) {
    $questionnaire = new Questionnaire(null,$_POST['name'], $_SESSION['user']->id, array());
    $_SESSION['questionnaire'] = $questionnaire;
    $_SESSION['questionnaire']->setToBd();
    header('Location: addQuestion.php?id='.$questionnaire->id);
    die();
}

include("head.html");?>
<title>profil</title>
<?php include("navbar.php");?>
<body>
	<main>
    <h1>Créer questionnaire</h1>
    <form action="createQuestionnaire.php" method="post">
        <label for="name">Nom du questionnaire</label>
        <input type="text" name="name" id="name" required>
        <input type="submit" value="Créer">
    </main>
</body>
</html>