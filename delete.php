<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: index.php');
	die();
}
// vérifie que l'utilisateur est bien le propriétaire du questionnaire
if (isset($_GET['id'])) {
    $questionnaire = Questionnaire::getFromBd($_GET['id']);
    if ($questionnaire->idCreateur == $_SESSION['user']->id) {
        Questionnaire::delFromBd($_GET['id']);
    }
    // les questions et réponses sont supprimées par la cascade
    header('Location: profil.php');
    die();
} 
?>