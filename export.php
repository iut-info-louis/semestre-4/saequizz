<?php
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: index.php');
	die();
}
if (isset($_GET['id'])) {
    $questionnaire = Questionnaire::getFromBd($_GET['id']);
    if ($questionnaire->idCreateur != $_SESSION['user']->id) {
        header('Location: profil.php');
        die();
    }
    else {
        $filename = "export.json";
        // questionnaire to json
        $returnstring = json_encode($questionnaire);
        // ecriture dans le fichier d'export
        $f = fopen($filename,"w");
        chmod($filename, 0777);
        fwrite($f, $returnstring);
        fclose($f);

        // envoie au user
        //Get file type and set it as Content Type
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        header('Content-Type: ' . finfo_file($finfo, $filename));
        finfo_close($finfo);

        //Use Content-Disposition: attachment to specify the filename
        header('Content-Disposition: attachment; filename='.basename($filename));

        //No cache
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        //Define file size
        header('Content-Length: ' . filesize($filename));

        ob_clean();
        flush();
        readfile($filename);
        // retour au profil
        header('Location: profil.php');
        die();
    }
}
