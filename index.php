<?php
// Start the session
session_start();

include("head.html");?>
<title>SAEQuizz</title>
<?php include("navbar.php");?>
<body>
	<main>
		<h1>SAEQuizz</h1>
		<p>SAEQuizz est un site de quizz en ligne. Il permet de créer des quizz et de les partager avec ses amis.</p>
		<p>Vous pouvez vous inscrire ou vous connecter pour accéder à vos quizz.</p>
	</main>
</body>
</html>