<?php
// Start the session
session_start();
if (isset($_SESSION['user'])) {
    header('Location: profil.php');
    die();
}
else{
    if (isset($_POST["mail"], $_POST["password"])){
        require_once("classes.php");
        $user = User::verifyUser($_POST["mail"], $_POST["password"]);
        if ($user){
            $_SESSION["user"] = $user;
            header('Location: profil.php');
            die();
        }
        else{
            $message = "<h2>Wrong mail or password</h2>";
        }
    }
}

include("head.html");?>
<title>Login</title>
<?php include("navbar.php");?>
<body>
    <main>
        <h1>Login</h1>
        <?php if(isset($message)){echo $message;} ?>
        <form method="post">
            <label for="mail">Mail</label>
            <input type="email" name="mail" id="mail">
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
            <input type="submit" value="Login">
        </form>
        <a href="register.php">Register</a>
    </main>
</body>
</html>