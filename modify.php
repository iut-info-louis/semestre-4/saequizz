<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: index.php');
	die();
}
// vérifie que l'utilisateur est bien le propriétaire du questionnaire
if (isset($_GET['id'])) {
    $questionnaire = Questionnaire::getFromBd($_GET['id']);
    if ($questionnaire->idCreateur != $_SESSION['user']->id) {
        header('Location: profil.php');
        die();
    }
    else {
        $_SESSION["questionnaire"] = $questionnaire;
    }

    if (isset($_POST["change"])){
        $_SESSION["questionnaire"]->nomQuestionnaire = $_POST["nomQ"];
        $_SESSION["questionnaire"]->setToBd();
    }
} else {
    header('Location: profil.php');
    die();
}

include("head.html");?>
<title>Modifier questionnaire</title>
<?php include("navbar.php");?>
<body>
	<main>
        <?php
        echo "<form action='modify.php?id=".$_SESSION['questionnaire']->id."' method='post'>";
        echo "<label>Nom questionnaire<input type='text' name='nomQ' value=".$_SESSION['questionnaire']->nomQuestionnaire."></label>";
        echo "<input type='submit' name='change' value='Change nom'>";
        echo "</form>";
        echo "<ol>";
        foreach($_SESSION["questionnaire"]->listeQuestions as $question){
            echo "<li><p>".$question->intitule." : <a href='modifyquestion.php?id=".$_SESSION['questionnaire']->id."&qid=".$question->id."'>Modify</a></p></li>";
        }
        echo "<li><a href='addQuestion.php'>Ajouter une question</a></li>";
        echo "</ol>";
        ?>
    </main>
</body>