<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: index.php');
	die();
}
// vérifie que l'utilisateur est bien le propriétaire du questionnaire
if (isset($_GET['id'])) {
    $questionnaire = Questionnaire::getFromBd($_GET['id']);
    if ($questionnaire->idCreateur != $_SESSION['user']->id) {
        header('Location: profil.php');
        die();
    }
    else {
        $_SESSION["questionnaire"] = $questionnaire;
        if (isset($_GET['qid'])){
            $question = Question::getQuestionFromBd($_GET['qid']);
        } else {
            header('Location: modify.php?id='.$_GET['id']);
            die();
        }
    }

    if (isset($_POST["change"])){
        $question->intitule = $_POST["nomQ"];
        foreach ($question->listeReponses as $rep) {
            $rep->texteReponse = $_POST[$rep->id];
            $rep->reponseValide = in_array($rep->id, $_POST['reponsesCorrectes'])?true:false;
            print_r($rep);
        }
        $question->setToBd($_SESSION["questionnaire"]->id);
        //header('Location: modify.php?id='.$_GET['id']);
        //die();
    }
} else {
    header('Location: profil.php');
    die();
}

include("head.html");?>
<title>Modifier questionnaire</title>
<?php include("navbar.php");?>
<body>
	<main>
        <?php
        echo "<form action='modifyquestion.php?id=".$_SESSION['questionnaire']->id."&qid=".$question->id."' method='post'>";
        echo "<label>Nom question<input type='text' name='nomQ' value=".$question->intitule."></label>";
        echo "<fieldset>";
        foreach ($question->listeReponses as $rep) {
            echo "<label><input type='text' name='".$rep->id."' value='".$rep->texteReponse."'></label>";
        }
        echo "</fieldset>";
        echo "<label for='reponsesCorrectes[]'>";
        foreach ($question->listeReponses as $rep) {
            $checked = "";
            if ($rep->reponseValide){
                $checked = "checked";
            }
            echo "<input type='checkbox' name='reponsesCorrectes[]' value='".$rep->id."' ".$checked.">";
        }
        echo "</label>";
        echo "<input type='submit' name='change' value='modifierquestion'>";
        echo "</form>";
        ?>
    </main>
</body>