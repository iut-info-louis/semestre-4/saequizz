<link rel="stylesheet" href="css/navbar.css">
</head>
<header>
	<h1>SAEQuizz</h1>
	<nav>
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="questionnaires.php">Questionnaires</a></li>
			<?php
			if (isset($_SESSION['user'])) {
				echo '<li><a href="profil.php">Profil</a></li>';}
			else{
				echo '<li><a href="login.php">Se connecter</a></li>';
			}
			?>
		</ul>
	</nav>
</header>
