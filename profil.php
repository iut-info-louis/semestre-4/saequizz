<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: index.php');
	die();
}

include("head.html");?>
<title>profil</title>
<?php include("navbar.php");?>
<body>
	<main>
		<h1>Profil</h1>
		<?php echo $_SESSION['user']; ?>
		<p>Mail: <?php echo $_SESSION['user']->email; ?></p>
		<ul>
			<?php
			$questionnaires = $_SESSION['user']->getQuestionnairesCree();
			foreach ($questionnaires as $q) {
				echo "<li>
				<ul>
					<li><p>".$q->nomQuestionnaire."</p></li>
					<li><a href='modify.php?id=".$q->id."'>Edit</a></li>
					<li><a href='delete.php?id=".$q->id."'>Delete</a></li>
					<li><a href='export.php?id=".$q->id."'>Export</a></li>
				</ul>";
			}
			?>
			<li><a href='createQuestionnaire.php'>Créer un questionnaire</a></li>
			<li><a href="import.php">Importer un questionnaire</a></li>
		</ul>
		<a href="logout.php">Logout</a>
	</main>
</body>
</html>
