<?php
// Start the session
require("classes.php");
session_start();

include("head.html");?>
<title>Questionnaires</title>
<?php include("navbar.php");?>
<body>
    <main>
        <h1>Liste des questionnaires</h1>
        <ul>
            <?php
            $questionnaires = Questionnaire::getQuestionnaires();
            foreach ($questionnaires as $q){
                echo "<li><a href='quizz.php?id=".$q->id."'>".$q->nomQuestionnaire."</a></li>";
            }
            ?>
        </ul>
    </main>
</body>
</html>