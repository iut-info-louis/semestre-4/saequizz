<?php
// Start the session
require("classes.php");
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: login.php');
	die();
}
if (isset($_GET['id'])) {
    $questionnaire = Questionnaire::getFromBd($_GET['id']);
    if (!$questionnaire) {
        header('Location: profil.php');
        die();
    }
    $_SESSION['questionnaire'] = $questionnaire;
} else {
    header('Location: questionnaires.php');
    die();
}


include("head.html");?>
<title>Questionnaires</title>
<?php include("navbar.php");?>
<body>
    <main>
        <h1>Questionnaire <?php echo $questionnaire->nomQuestionnaire; ?></h1>
        <form action="correction.php?id=<?php echo $questionnaire->id; ?>" method="post">
            <?php
            foreach ($questionnaire->listeQuestions as $q){
                if ($q->typeQuestion->nomTypeQuestion == "QCU") {
                    echo "<fieldset>";
                    echo "<legend>".$q->intitule."</legend>";
                    foreach ($q->listeReponses as $r) {
                        echo "<label>".$r->texteReponse."<input type='radio' name='".$q->id."' value='".$r->id."'></label>";
                    }
                    echo "</fieldset>";
                } else if ($q->typeQuestion->nomTypeQuestion == "QCM") {
                    echo "<fieldset>";
                    echo "<legend>".$q->intitule."</legend>";
                    foreach ($q->listeReponses as $r) {
                        echo "<label>".$r->texteReponse."<input type='checkbox' name='".$q->id."[]' value='".$r->id."'></label>";
                    }
                    echo "</fieldset>";
                }
            }
            ?>
            <input type="submit" value="Valider">
    </main>
</body>
</html>