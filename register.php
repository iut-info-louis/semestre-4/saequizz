<?php
// Start the session
require("classes.php");
session_start();
if (isset($_SESSION['user'])) {
    header('Location: profil.php');
    die();
}
else{
    if (isset($_POST["mail"])){
        if(User::checkExist($_POST["mail"])){
            $message = "<h2>L'adresse mail est déjà utilisée</h2>";
        }
        else {
            $user = User::registerUser($_POST["mail"], $_POST["password"]);
            $_SESSION["user"] = $user;
            header('Location: profil.php');
            die();
        }
    }
}

include("head.html");?>
<title>Register</title>
<?php include("navbar.php");?>
<body>
    <main>
        <h1>Register</h1>
        <?php if (isset($message)){echo $message;}?>
        <form method="post">
            <label for="mail">Mail</label>
            <input type="email" name="mail" id="mail">
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
            <input type="submit" value="Submit">
        </form>
        <a href="login.php">Login</a>
    </main>
</body>
</html>